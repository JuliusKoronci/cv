<?php

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Hello :)
 *
 * My name is  Martina Koronci Babinská. I would like to apply for the position: Junior PHP Developer.
 * As you can see in my curriculum, I don't have as much experience in programming, but I like it very much,
 * I used to program during my school and I am working like a full time Junior Web Developer for the last 7 months.
 * And .. what is the most important part and what is the reason why I am applying for this possition is: I love Mini
 * Games :)
 */
$myCv = new \Slovakia\Bratislava\CurriculumVitae('Martina', 'Koronci Babinská', 'mb@web-solutions.sk', '00421 904 444 085');

/**
 * Set Ph.D. study
 */
$myCv->addEducation('Ph.D.',[
    'university'       => 'Comenius University in Bratislava',
    'faculty'          => 'Faculty of Mathematics, Physics and Informatics',
    'start'            => '09.2011',
    'end'              => '08.2015',
    'programmeOfStudy' => 'The theory of mathematics education',
    'thesis'           => 'The real context of mathematical problems as a motivating factor',
    'goalOfTheThesis'  => 'Research of attractiveness of real-life mathematical problems based on medical context for
                           senior students of secondary schools and lower grades of high schools',
]);

/**
 * Set Masters degree study
 */
$myCv->addEducation('Masters degree',[
    'university'       => 'Comenius University in Bratislava',
    'faculty'          => 'Faculty of Mathematics, Physics and Informatics',
    'start'            => '09.2009',
    'end'              => '06.2011',
    'programmeOfStudy' => 'Management mathematics',
    'thesis'           => 'E-learning support of education of mathematics for graduate and university students 
                          (thematic unit: functions)',
    'goalOfTheThesis'  => 'Create and prove the usability of e-learning support, which is assigned to support learning 
                           of functions in mathematics (this is available in Slovak language at: 
                           http://elearn.ematik.fmph.uniba.sk/~babinska/)',
]);

/**
 * Set Bachelor degree study
 */
$myCv->addEducation('Bachelor degree',[
    'university'       => 'Comenius University in Bratislava',
    'faculty'          => 'Faculty of Mathematics, Physics and Informatics',
    'start'            => '09.2005',
    'end'              => '06.2009',
    'programmeOfStudy' => 'Management mathematics',
    'thesis'           => 'Research and development of companies and their impact on the market structure',
]);

/**
 * Set secondary school study
 */
$myCv->addEducation('Secondary school',[
    'school'           => 'Secondary school, Gymnazium of Martin Hattala in Trstená',
    'start'            => '09.1997',
    'end'              => '05.2005',
    'programmeOfStudy' => 'Informatics',
]);

/**
 *Set Work experience in programming
 */
$myCv->addWorkExperience('Programming',[
    'position'    => 'Junior PHP WEB Developer',
    'start'       => '11.2015',
    'end'         => 'present',
    'employer'    => 'WEB-SOLUTIONS',
    'description' => 'I\'am working like Junior Developer for a company named WEB-SOLUTIONS. The main part of my work 
                        include\'s working on the following projects:
                         Scrum JET - a tool for agile development (HTML, CSS, Symfony, Bootstrap, uikit)
                          (http://www.scrumjet.com/)
                         Kelta BLOG - web scrapper: feeding the blog with external articles (Symfony, DOM Document)
                          (http://www.kelta.com)
                         Knowledge portal - System for managing web-resources (HTML, CSS, Symfony, Bootstrap, DOM Document)
                          (http://kp2.web-solutions.sk/)
                         LanHelpdesk - internal Helpdesk application for an IT company 
                          (HTML, CSS, Nette Framework, Bootstrap, AJAX, Javascript, Doctrine)',
])->addWorkExperience('Programming',[
    'position'    => 'Part time Junior WEB Developer during study',
    'start'       => '01.2011',
    'end'         => '01.2015',
    'description' => 'I worked on e-commerce solutions and personal pages during my study:
                         Tea Gardens - (HTML, CSS, Nette Framework, Sussy, Doctrine)
                          (http://teagardens.nl/)
                         Real math in medicine - an edducational application developed in MeteorJS 
                         U Chorej Vrany - (HTML, CSS, Nette Framework, Bootstrap, Doctrine)
                          (http://obchodik.com/)
                         Pravý Šafrám - (HTML, CSS, Nette Framework, Bootstrap, Doctrine)
                          (http://pravysafran.sk/)
                         Arganmidas - (HTML, CSS, Nette Framework, Bootstrap, Doctrine)',
]);

/**
 *Set Work experience in math
 */
$myCv->addWorkExperience('Math',[
    'position'    => 'International Measurements Analyst',
    'start'       => '09.2014',
    'end'         => '11.2015',
    'employer'    => 'National Institute for certified educational measurements (NÚCEM),
                        Department of International Measurements (OMM)',
    'description' => 'This work gave me an opportunity to participate on quantitative research. The main part of my 
                        work included:
                         analysis of data from International Survey TALIS (The OECD Teaching and Learning International Survey),
                         preparation of National Report for Slovak Republic from International Survey TALIS,
                         working with SPSS software, IDB Analyzer.',
])->addWorkExperience('Math',[
    'position'    => 'Scientific assistant',
    'start'       => '09.2013',
    'end'         => '08.2014',
    'employer'    => 'Comenius University in Bratislava, Faculty of Mathematics, Physics and Informatics',
    'description' => 'The main part of my work included:
                         teaching mathematical analysis of students at first grade at University,
                         working on my PhD thesis.',
]);

/**
 * Set professional skills in programming
 */
$myCv->addSkillSet('Programming', [
    'PHP', 'OOP',
    'HTML', 'CSS',
    'ERD', 'Doctrine', 'MySQL',
    'Symfony Framework', 'Nette Framework', 'MVC',
    'PhpStorm', 'GIT', 'SASS',
    'JavaScript', 'SQL',
]);

/**
 * Set language skills
 */
$myCv->addSkillSet('Languages', [
    'Slovak'  => 'mother tongue',
    'English' => 'active: read, write',
]);

/**
 * Set personal skills
 */
$myCv->addSkillSet('Personal', [
    'Analytical thinking'  => 'Thank \'s to mathematical education :).',
    'Meeting goals'        => 'I like challenges and the good feeling after achieving them.',
    'Interest in learning' => 'I like learning, reading and acquiring new knowledge and experience.',
]);

/**
 * Set interests
 */
$myCv->addInterest('programming')
    ->addInterest('PC Mini Games like f.i. Delicious Emily :)')
    ->addInterest('desk Games')
    ->addInterest('books')
    ->addInterest('teaching and working with students')
    ->addInterest('art')
    ->addInterest('cycling');

/**
 * Set Additional Info - publications
 */
$myCv->addAdditionalInfo('Publication', [
    'name'        => 'How much of math is in one medical examination? Unconventional set of tasks',
    'author'      => 'Martina Koronci Babinská',
    'description' => 'Instructional booklet for secondary school and university students',
    'publisher'   => 'FMFI UK, Knižničné a edičné centrum, Bratislava',
    'ISBN'        => '978-80-8147-028-8',
    'date'        => '04.2015',
]);

/**
 * Set Additional Info - conferences
 */
$myCv->addAdditionalInfo('Conference', [
    'name'         => 'International conference – Setkání učitelů matematiky všech typů a stupňů škol,
                        Srní, Czech Republic',
    'contribution' => 'The real use of mathematics as motivation for students',
    'publication'  => 'In: Setkání učitelů matematiky všech typů a stupňů škol 2014 [electronic source]. - Plzeň :
                        Vydavatelský servis, 2014. - P. 43-48 [CD-ROM]. - ISBN 978-80-86843-46-9',
    'date'         => '11.2014',
])->addAdditionalInfo('Conference', [
    'name'         => 'National student conference – Student scientific conference, FMFI UK, Bratislava, Slovak Republic',
    'contribution' => 'Can reality motivate students to study mathematics?',
    'publication'  => 'In: Študentská vedecká konferencia FMFI UK, Bratislava 2013: Zborník príspevkov. - Bratislava : 
                        Fakulta matematiky, fyziky a informatiky UK, 2013. - P. 396-400. - ISBN 978-80-8147-009-7',
    'date'         => '04.2013',
    'award'        => 'Price of Literary Fund',
])->addAdditionalInfo('Conference', [
    'name'     => 'National conference – Otvorený softvér vo vzdelávaní, výskume a v IT riešeniach (OSSConf), 
                        Žilina, Slovak Republic',
    'function' => 'Participation in the organizing of the GeoGebra section, leading workshops about using
                        GeoGebra as helping software for teaching',
    'date'     => '07.2012',
])->addAdditionalInfo('Conference', [
    'name'         => 'International conference – History of Mathematics and Teaching of Mathematics, Sárospatak, Hungary',
    'contribution' => 'E-learning support of education of mathematics (thematic unit: functions)',
    'publication'  => 'In: Proceedings of the Conference on the History of Mathematics and Teaching of Mathematics
                        [electronic source]. - Miskolc : University of Miskolc, 2012. – no p. [8 p.] [CD-ROM]. - ISBN
                        978-963-661-988-6',
    'date'         => '05.2012',
])->addAdditionalInfo('Conference', [
    'name'         => 'International student conference – Česko-slovenská přehlídka SVOČ 2011, Didaktika matematiky, 
                        Banská Bystrica, Slovak Republic',
    'contribution' => 'E-learning support of education of mathematics for graduating students and university students',
    'publication'  => 'In: Anotace prací prezentovaných na SVOČ 2011 [electronic source]. - [Praha] : Společnost
                        učitelů matematiky JČMF, 2011. – [no p.] , 1 p. [online]',
    'date'         => '06.2011',
    'award'        => 'Honorable mention',
])->addAdditionalInfo('Conference', [
    'name'         => 'National student conference – Student scientific conference, FMFI UK, Bratislava, Slovak Republic',
    'contribution' => 'E-learning support of education of mathematics for graduating students and university students
                        (thematic unit: functions)',
    'publication'  => 'In: Študentská vedecká konferencia FMFI UK, Bratislava 2011 : Zborník príspevkov. - Bratislava : 
                        Fakulta matematiky, fyziky a informatiky UK, 2011. - P. 380-391. - ISBN 978-80-89186-87-7',
    'date'         => '04.2011',
    'award'        => 'Winner',
])->addAdditionalInfo('Conference', [
    'name'         => 'International student conference – Česko-slovenská soutež prací z didaktiky matematiky a 
                        informační výchovy ŠVOČ, Kostelec nad Černými lesy, Czech Republic',
    'contribution' => 'The status and opportunities of e-learning support for all types of schools in Slovakia',
    'publication'  => 'In: Anotace prací prezentovaných na SVOČ 2010 [electronic source]. - [Praha] : Společnost
                        učitelů matematiky JČMF, 2010. - [no p.] , 1 p. [online]',
    'date'         => '06.2010',
    'award'        => '2. place',
])->addAdditionalInfo('Conference', [
    'name'         => 'National student conference – Student scientific conference, FMFI UK, Bratislava, Slovak Republic',
    'contribution' => 'The status and opportunities of e-learning support for all types of schools at Slovakia',
    'publication'  => 'In: Študentská vedecká konferencia FMFI UK, Bratislava 2010 : Zborník príspevkov. - Bratislava : 
                        Fakulta matematiky, fyziky a informatiky UK, 2010. - P. 358-369. - ISBN 978-80-89186-68-6',
    'date'         => '04.2010',
    'award'        => 'Winner',
]);

$myCv->printCurriculumVitae($myCv);