<?php
namespace Slovakia\Bratislava;

/**
 * Interface WorkExperienceInterface
 */
interface WorkExperienceInterface
{
    /**
     * @return array
     */
    public function getWorkExperience();

    /**
     * Add an Experience item to the list of Experience history
     *
     * @param  string $type
     * @param array   $experience
     *
     * @return $this
     */
    public function addWorkExperience($type, array $experience);
}