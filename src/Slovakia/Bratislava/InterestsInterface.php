<?php
namespace Slovakia\Bratislava;

/**
 * Interface InterestsInterface
 */
interface InterestsInterface
{
    /**
     * @return array
     */
    public function getInterests();

    /**
     * Add an Interest item to the list of Interests
     *
     * @param string $interest
     *
     * @return $this
     */
    public function addInterest($interest);
}